<?php

use Booking\BookingManager;
use Booking\Config;
use Nette\Database\Connection;
use Nette\Database\Context;
use Nette\Database\Structure;

require __DIR__ . "/../vendor/autoload.php";

define('DS', DIRECTORY_SEPARATOR);
define('DATABASE_FILE', __DIR__ . DS . 'database.db3');
define('TMP_DIRECTORY', __DIR__ . DS . 'tmp');

// db connection
$connection = new Connection('sqlite:' . DATABASE_FILE, '', '');
$fileStorage = new Nette\Caching\Storages\FileStorage(TMP_DIRECTORY);
$structure = new Structure($connection, $fileStorage);
$database = new Context($connection, $structure);

try {
    $bookingManager = new BookingManager(new Config(), $database);
    $bookingManager->reset();

    $from = new DateTime('now - 7 days');
    $to = new DateTime('now - 5 days');
    $numberOfPersons = 7;

    $roomsCombinations = $bookingManager->getModel()->getRoomsCombinations($from, $to, $numberOfPersons);

    // persons per date before booking
    var_dump('personsPerDate', $bookingManager->getCalendar()->personsPerDate());

    $realReservation = $bookingManager->getReservation()->makeReservation(
        $from,
        $to,
        $numberOfPersons,
        current($roomsCombinations)->getArrayCopy()
    );
    var_dump("reservation result: " . $realReservation['reservation']);

    // occupient rooms
    var_dump('occupied rooms', $realReservation['occupied_rooms']);

    // persons per date after booking
    var_dump('personsPerDate', $bookingManager->getCalendar()->personsPerDate());

    $bookingManager->getModel()->checkFreeRooms()

    //var_dump(current($roomsCombinations)->getArrayCopy());

} catch (\Exception $e) {
    echo $e->getMessage();
}
