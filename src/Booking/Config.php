<?php

namespace Booking;

class Config
{
    /** @const int */
    const MINIMAL_ROOM_CAPACITY = 2;

    /** @var array */
    private $rooms = [
        // quantity => capacity
        2 => 2,
        2 => 3,
        3 => 4
    ];

    /**
     * @return array
     */
    public function getRooms() {
        return $this->rooms;
    }

    /**
     * @return int
     */
    public function getMinimalRoomCapacity() {
        return self::MINIMAL_ROOM_CAPACITY;
    }
}