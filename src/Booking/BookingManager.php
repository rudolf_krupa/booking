<?php

namespace Booking;

use Booking\Models\BookingModel;
use Booking\Models\Calendar;
use Booking\Models\Reservations;
use Nette\Database\Context;

class BookingManager
{
    /** @var Config */
    private $config;

    /** @var Context */
    private $driver;

    /** @var BookingModel */
    private $bookingModel;

    /**
     * @param Config $config
     * @param Context $driver
     */
    public function __construct(Config $config, Context $driver)
    {
        $this->config = $config;
        $this->driver = $driver;

        $this->bookingModel = new BookingModel($config, $driver);
    }

    public function reset() {
        $this->bookingModel->reset();
    }

    /**
     * @return BookingModel
     */
    public function getModel() {
        return $this->bookingModel;
    }

    /**
     * @return Calendar
     */
    public function getCalendar() {
        return new Calendar($this->driver);
    }

    /**
     * @return Reservations
     */
    public function getReservation() {
        return new Reservations($this->driver);
    }
}