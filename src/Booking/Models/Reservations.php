<?php

namespace Booking\Models;

use DateInterval;
use DateTime;
use DatePeriod;
use Exception;
use Nette\InvalidArgumentException;

class Reservations extends BaseModel {

    /** @var string */
    public $table = 'reservations';

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param int $numberOfPersons
     * @param array $roomsCombination
     * @return array
     * @throws InvalidArgumentException|Exception
     */
    public function makeReservation(DateTime $from, DateTime $to, $numberOfPersons = 1, array $roomsCombination) {

        if($numberOfPersons <= 0) {
            throw new InvalidArgumentException("Number of person is required");
        }

        if(empty($roomsCombination)) {
            throw new InvalidArgumentException("Room combination is required");
        }

        $from = clone $from;
        $to = clone $to;

        $data = \Nette\Utils\ArrayHash::from([]);
        $data->from = $from->format('Y-m-d');
        $data->to = $to->format('Y-m-d');

        $data->firstname = 'firstname';
        $data->surname = 'surname';
        $data->email = 'email';
        $data->phone = 'phone';
        $data->note = 'note';

        $data->active = 1;

        if($result = $this->driver->table($this->table)->insert($data)) {

            $lastId = $this->driver->getInsertId($this->table);
            $result = $this->driver->table($this->table)->get($lastId);

            // insert into `reservations_calendar`
            $interval = new DateInterval('P1D');
            $daterange = new DatePeriod($from, $interval, $to);

            $reservationInCalendar = [];
            foreach($daterange as $date) {
                $reservationInCalendar[] = [
                    'reservations_id' => $result->id,
                    'date' => $date->format('Y-m-d'),
                    'from' => $from->format('Y-m-d'),
                    'to' => $to->format('Y-m-d'),
                    'number_of_persons' => $numberOfPersons
                ];
            }
            
            if(empty($reservationInCalendar))
                throw new Exception ('no data for reservations_calendar -> bad calendar input');  
            
            $this->driver->query(sprintf("INSERT INTO `%s`", 'reservations_calendar'), $reservationInCalendar);

            // room combinations
            //$roomsCombinationArray = explode('|', $roomsCombination);

            $roomOccupationCalendar = [];
            
            $roomOccupationCalendarData = [];
            
            if(count($roomsCombination) > 0) {
                foreach($roomsCombination as $room) {
                    foreach($daterange as $date) {
                        $roomOccupationCalendar[] = [
                            'reservations_id' => $result->id,
                            'key' => $room->key,
                            'date' => $date->format('Y-m-d'),
                            'from' => $from->format('Y-m-d'),
                            'to' => $to->format('Y-m-d')
                        ];
                        
                        $roomOccupationCalendarData[$room->key] = $room->key;
                    }        
                }
            }
                        
            $this->driver->query(sprintf("INSERT INTO `%s`", 'room_occupation'), $roomOccupationCalendar);

            foreach($this->driver->table("rooms") as $room) {
                if(isset($roomOccupationCalendarData[$room->key]))
                    $roomOccupationCalendarData[$room->key] = $room->toArray();
            }
            
            return [
                "reservation" => $result,
                'occupied_rooms' => $roomOccupationCalendarData
            ];
        }                
    }
    
    public function getAll() {
        return $this->driver->table($this->table)->order('from');
    }
    
    public function deactivateReservation($id) {
        if($reservation = $this->driver->table($this->table)->where('id = ?', $id)) {
            if($reservation->update(array("active" => 0))) {
                return true;
            }            
        } 
        return false;
    }
    
    public function activateReservation($id) {
        if($reservation = $this->driver->table($this->table)->where('id = ?', $id)) {
            if($reservation->update(["active" => 1])) {
                return true;
            }            
        } 
        return false;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getReservationRooms(int $id) {
        $rooms = [];
        foreach($this->driver->table('room_occupation_view')->where("reservations_id = ?", (int) $id) as $reservationRoom) {
            $rooms[$reservationRoom->key] = [
                'room_number' => $reservationRoom->room_number,
                'persons' => $reservationRoom->persons
            ];
        }
        return $rooms;
    }
}
