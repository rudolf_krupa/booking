<?php

namespace Booking\Models;

use Nette\Database\Context;

abstract class BaseModel {

    /** @var Context */
    protected $driver;

    /**
     * @param Context $driver
     */
    public function __construct(Context $driver)
    {
        $this->driver = $driver;
    }
}
