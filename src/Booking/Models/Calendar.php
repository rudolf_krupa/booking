<?php

namespace Booking\Models;

use DateTime;

class Calendar extends BaseModel {

    /**
     * @return array
     */
    private function get() {
        // count used capacity by persons in occuped rooms
        $result = $this->driver->query('        
                    SELECT sum(room_occupation_view.persons) AS presons_per_date, room_occupation_view.date AS date
                    FROM room_occupation_view 
                    WHERE room_occupation_view.active = 1 
                    GROUP BY room_occupation_view.date
                ');

        return $result ?? [];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function personsPerDate() {
        foreach($this->get() as $datePlusPerson) {

            if(!isset($personsPerDate))
                $personsPerDate = [];
            
            $datePlusPerson->date = new DateTime($datePlusPerson->date);
            $personsPerDate[$datePlusPerson->date->format('Y-m-d')] = $datePlusPerson->presons_per_date;            
        }

        $result = [];
        if(isset($personsPerDate))
            $result = $personsPerDate;

        return $result;
    }
}
