<?php

namespace Booking\Models\Entities;

use stdClass;

class Room extends stdClass {

    /** @var int */
    public $persons;
    /** @var int */
    public $free = true;

    /** @var string */
    public $key;

    /** @var int */
    public static $order;

    /**
     * @param int $persons
     */
    public function __construct(int $persons) {
        $this->persons = $persons;

        self::$order++;
        
        $this->key = $this->persons . '-' . self::$order;
    }

    /**
     * @return bool
     */
    public function isFree() {
        return (bool) $this->free;
    }

    /**
     * @param bool $value
     */
    public function setFree(bool $value) {
        $this->free = $value;
    }
}

