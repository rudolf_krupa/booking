<?php

namespace Booking\Models\Entities;

use ArrayObject;

class Combination extends ArrayObject {

    /** @var int */
    public $persons = 0;

    /**
     * @param int $requestPersons
     * @return bool
     */
    public function isCorrect($requestPersons) {
        
        $persons = 0;
        foreach($this as $room)
            $this->persons = $persons = $persons + $room->persons;
        
        if($persons < $requestPersons) {
            return false;
        }

        $room = 1;
        $roomsInCombination = $this->count();

        $this->ksort();
        foreach($this as $itemKey => $itemValue) {
            
            if($persons > $requestPersons && $room != $roomsInCombination)
                return false;
            
            $room++;
        }
        
        return true;
    }

    /**
     * @return int
     */
    public function getNumberOfRooms() {
        return $this->count();
    }

    /**
     * @return array
     */
    public function getRoomsPersonsList() {
        $roomsPersons = [];
        if(count($this) > 0)
            foreach($this as $room)
                $roomsPersons[] = $room->persons;
        
        return $roomsPersons;
    }

    /**
     * @return array
     */
    public function getRoomsKeys() {
        $roomsKeys = [];
        if(count($this) > 0)
            foreach($this as $room)
                $roomsKeys[] = $room->key;
        
        return $roomsKeys;
    }
}
