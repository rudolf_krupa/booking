<?php

namespace Booking\Models;

use Booking\Config;
use Booking\Models\Entities\Combination;
use Booking\Models\Entities\Room;
use DateTime;
use Exception;
use Nette\Database\Context;
use Nette\InvalidArgumentException;

class BookingModel extends BaseModel {

    /** @var array */
    public $rooms = [];
    /** @var array */
    public $freeRooms = [];

    /** @var DateTime */
    public $from;
    /** @var DateTime */
    public $to;

    /** @var Config */
    private $config;

    /**
     * @param Config $config
     * @param Context $driver
     */
    public function __construct(Config $config, Context $driver) {

        parent::__construct($driver);

        $this->config = $config;
        foreach($this->config->getRooms() as $quantity => $capacity) {

            for($i = 1; $i <= $quantity; $i++) {
                $this->rooms[] = new Room($capacity);
            }
        }
    }

    public function reset()
    {
        // db tables and views
        $this->driver->query('DROP TABLE IF EXISTS `reservations`');
        $this->driver->query('
            CREATE TABLE `reservations` (
            `id` INTEGER PRIMARY KEY  NOT NULL, 
            `firstname` TEXT NOT NULL, 
            `surname` TEXT NOT NULL,  
            `from` DATETIME NOT NULL, 
            `to` DATETIME NOT NULL, 
            `number_of_persons` INTEGER DEFAULT 0,
            `email` TEXT NOT NULL, 
            `phone` TEXT NOT NULL, 
            `note` TEXT,
            `active` INTEGER DEFAULT 0
        )
        ');

        $this->driver->query('DROP TABLE IF EXISTS `reservations_calendar`');
        $this->driver->query('
            CREATE TABLE `reservations_calendar` (
            `reservations_id` INTEGER NOT NULL,
            `date` DATETIME NOT NULL,
            `from` DATETIME NOT NULL, 
            `to` DATETIME NOT NULL, 
            `number_of_persons` INTEGER DEFAULT 0
        )
        ');

        $this->driver->query('DROP TABLE IF EXISTS `room_occupation`');
        $this->driver->query('
            CREATE TABLE `room_occupation` (
            `reservations_id` INTEGER NOT NULL,
            `key` TEXT NOT NULL,
            `date` DATETIME NOT NULL,
            `from` DATETIME NOT NULL, 
            `to` DATETIME NOT NULL
        )
        ');

        $this->driver->query('DROP TABLE IF EXISTS `rooms`');
        $this->driver->query('
            CREATE TABLE `rooms` (
            `id` INTEGER PRIMARY KEY NOT NULL,
            `room_number` INTEGER NOT NULL,
            `key` TEXT NOT NULL,
            `persons` INTEGER NOT NULL
            )
        ');
        // create rooms list in database
        $this->createListOfRooms();

        $this->driver->query('DROP VIEW IF EXISTS `room_occupation_view`');
        $this->driver->query('
            CREATE VIEW `room_occupation_view` AS
            SELECT `room_occupation`.*, `rooms`.`room_number`, `rooms`.`persons`, `reservations`.`active`
            FROM `room_occupation` 
            JOIN `reservations` ON `reservations`.`id` = `room_occupation`.`reservations_id`
            JOIN `rooms` ON `rooms`.`key` = `room_occupation`.`key`
        ');
    }

    private function createListOfRooms() {
        $this->driver->query('DELETE FROM `rooms`');
        $this->driver->query('VACUUM');

        $roomNumber = 1;
        foreach($this->getRoomsList() as $room) {
            $this->driver->table('rooms')->insert([
                'key' => $room->key,
                'persons' => $room->persons,
                'room_number' => $roomNumber
            ]);
            $roomNumber++;
        }
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @param int $requestPersons
     * @return array|bool
     * @throws Exception
     */
    public function getRoomsCombinations(DateTime $from, DateTime $to, int $requestPersons) {
        $this->setFromAndTo($from, $to);
        return $this->getRoomsForPersons($requestPersons);
    }

    /**
     * @param DateTime $from
     * @param DateTime $to
     * @throws Exception
     */
    private function setFromAndTo(DateTime $from, DateTime $to) {
        // exclude rooms by dates
        if(!empty($from) || !empty($to)) {
            $this->from = $from;
            $this->to = $to;
        } else {
            throw new Exception("Set from and to");
        }
    }

    /**
     * @param int $requestPersons
     * @return array
     */
    public function getRoomsForPersons(int $requestPersons) {

        if($requestPersons <= 0) {
            throw new InvalidArgumentException("Number of person is required");
        }

        // minimal size of room
        if($requestPersons == 1)
            $requestPersons = $this->config->getMinimalRoomCapacity();

        $this->getFreeRooms();

        $combinations = $this->getCombination();

        $possibleCombinations = [];
        foreach($combinations as $combinationKey => $combination) {
            if($combination->isCorrect($requestPersons))
                $possibleCombinations[$combinationKey] = $combination;
        }

        // filter if has same persons as requested presons
        $tmpPossibleCombinations = [];
        foreach($combinations as $combinationKey => $combination) {
            if(isset($combination->persons) && $combination->persons == $requestPersons) {
                $tmpPossibleCombinations[$combinationKey] = $combination;
            }
        }

        if(count($tmpPossibleCombinations) > 0) {
            $possibleCombinations = $tmpPossibleCombinations;
        }

        // group by persons and number of rooms
        $tmpPossibleCombinations = [];
        if(count($possibleCombinations) > 0) {
            foreach($possibleCombinations as $combination) {
                if(!isset($tmpPossibleCombinations))
                    $tmpPossibleCombinations = [];

                $roomKey = $combination->getNumberOfRooms()  . '----' . implode('/', $combination->getRoomsPersonsList());
                $tmpPossibleCombinations[$roomKey] = $combination;
            }
        }
        if(count($tmpPossibleCombinations) > 0) {
            $possibleCombinations = $tmpPossibleCombinations;
        }

        return $possibleCombinations;
    }

    private function getRoomsList() {
        return $this->rooms;
    }

    public function getFullCapacity() {
        $capacity = 0;
        
        if(count($this->rooms) > 0)
            foreach($this->rooms as $room)
                $capacity = $capacity + $room->persons;
        
        return $capacity;
    }
    
    public function checkFreeRooms() {
        if(empty($this->driver))
            throw new Exception('Check rooms need database');

        if(!($this->from instanceof DateTime) || !($this->from instanceof DateTime))
            throw new Exception('"from" or "to" is not set');

        if(($this->from instanceof DateTime) || ($this->from instanceof DateTime)) {
            $results = $this->driver->table('room_occupation_view')->where(sprintf('`active` = 1 AND `date` BETWEEN "%s" AND "%s"', $this->from->format('Y-m-d'), $this->to->format('Y-m-d')))->group('key');

            foreach($results as $usedRoom) {
                foreach($this->rooms as $room) {
                    if($room->key == $usedRoom->key) 
                        $room->setFree(0);
                }
            }
        }
    }
    
    /**
     * @return array
     */
    private function getFreeRooms() {
        foreach($this->rooms as $roomKey => $roomObject) {
            if($roomObject->isFree())
                $this->freeRooms[$roomKey] = $roomObject;
        }
                
        return $this->freeRooms;
    }

    /**
     * @param bool $removeEmpty
     * @return array
     */
    public function getCombination($removeEmpty = true) {
        // initialize by adding the empty set
        $results = [[]];
        
        foreach ($this->freeRooms as $element)
            foreach ($results as $combination)
                array_push($results, array_merge(array($element), $combination));
           
        // remove first array element
        $currentKey = key(current($results));
        if(!empty($currentKey) && count($results[$currentKey]) == 0 && $removeEmpty) {
            unset($results[$currentKey]);
        }
        
        // convert to object "arrayObject"
        if(count($results) > 0) {
            foreach($results as $resutlKey => $resultItem) {
                $results[$resutlKey] = new Combination($resultItem);                
            }
        }
        
        return $results;
    }
} 
